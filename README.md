## Wordstream AdSet GeoLocations

### Overview

This solution is written in Java (version 8) and leverages a few different tools for additional support:

* Spring - dependency injection and REST endpoint/data bindings
* Spring Boot - a framework for creating standalone Spring applications
* Facebook Business SDK - for parsing AdSet data
* Gson - JSON-object mapping (used by Facebook)
* Jackson - JSON-object mapping (used by Spring)
* Lombok - using annotations to generate getters, setters, constructors, etc.
* Gradle - build management

The instructions for getting up and running are below. *Please let me know if there are any questions!*

### Build/Run Instructions

##### Building the jar:
```
./gradlew bootJar
```

##### Running the app (port 8080)
```
./gradlew bootRun
```

or 
```
java -jar build/libs/wordstream.jar
```

### HTTP Requests and Responses
```
GET localhost:8080/adset/geolocations/top
```

The result is a JSON list of objects containing a geolocation object and the number of occurrences, sorted in
descending order. 

e.g.
```
[
  {
    "geoLocation": { ... },
    "count": <value>
  },
  ...
]
```

There is also support for an optional query parameter "limit" to control the number of values in the result

```
GET localhost:8080/adset/geolocations/top?limit=10
``` 

The service interface is defined in AdSetServiceInterface.java

### Tests

A different set of tests was created for each layer (controller, service, data access) and can 
be invoked with the following command:

```
./gradlew clean test
```

