package com.mdl.wordstream.dao;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.mdl.wordstream.exception.WordstreamAdSetDataException;
import com.mdl.wordstream.model.AdSetListWrapper;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

/**
 * Fetch and parse the ad set data from the given URL.
 */
@Repository
public class WordstreamAdSetDAO {

    private static final String DATA_URL = "https://app.wordstream.com/services/v1/wordstream/interview_data";

    public AdSetListWrapper fetchAdSetData() throws WordstreamAdSetDataException {
        try {
            InputStreamReader reader = getInputStreamReaderForUrl(DATA_URL);
            JsonElement json = new JsonParser().parse(reader);
            return parseAdSetFromJson(json);
        } catch (IOException e) {
            throw new WordstreamAdSetDataException("Unable to fetch wordstream ad set data", e);
        }
    }

    private InputStreamReader getInputStreamReaderForUrl(String dataUrl) throws IOException {
        URL url = new URL(dataUrl);
        URLConnection request = url.openConnection();
        request.connect();
        return new InputStreamReader((InputStream) request.getContent());
    }
    
    AdSetListWrapper parseAdSetFromJson(JsonElement json) {
        return new Gson().fromJson(json, AdSetListWrapper.class);
    }
}
