package com.mdl.wordstream.controller;

import com.mdl.wordstream.AdSetServiceInterface;
import com.mdl.wordstream.dto.TopGeoLocation;
import com.mdl.wordstream.service.AdSetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


/**
 * An implementation of the AdSet service - handle any AdSet REST requests here.
 */
@RestController
public class AdSetController implements AdSetServiceInterface {

    private final AdSetService adSetService;

    @Autowired
    public AdSetController(AdSetService adSetService) {
        this.adSetService = adSetService;
    }

    @Override
    public List<TopGeoLocation> getMostFrequentGeoLocations(Integer limit) throws Exception {
        return adSetService.findTopGeoLocations(limit);
    }
}
