package com.mdl.wordstream.transformer;

import com.facebook.ads.sdk.TargetingGeoLocation;
import com.google.gson.Gson;
import com.mdl.wordstream.dto.GeoLocation;
import org.springframework.stereotype.Component;

/**
 * Transform the Facebook SDK's TargetingGeoLocation into a local representation of the same data
 * to provide control over the serialization details and enable equality/comparison checking.
 */
@Component
public class GeoLocationTransformer {

    private Gson gson = new Gson();

    public GeoLocation toGeoLocation(TargetingGeoLocation targetingGeoLocation) {
        String json = gson.toJson(targetingGeoLocation);
        return gson.fromJson(json, GeoLocation.class);
    }
}
