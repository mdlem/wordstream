package com.mdl.wordstream.exception;

public class WordstreamAdSetDataException extends Exception {

    public WordstreamAdSetDataException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
