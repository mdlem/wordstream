package com.mdl.wordstream.service;

import com.facebook.ads.sdk.AdSet;
import com.facebook.ads.sdk.Targeting;
import com.facebook.ads.sdk.TargetingGeoLocation;
import com.mdl.wordstream.dao.WordstreamAdSetDAO;
import com.mdl.wordstream.dto.GeoLocation;
import com.mdl.wordstream.dto.TopGeoLocation;
import com.mdl.wordstream.exception.WordstreamAdSetDataException;
import com.mdl.wordstream.model.AdSetListWrapper;
import com.mdl.wordstream.transformer.GeoLocationTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * A service for fetching the data and identifying the top geo locations.
 */
@Service
public class AdSetService {

    private final WordstreamAdSetDAO wordstreamAdSetDAO;
    private final GeoLocationTransformer geoLocationTransformer;

    @Autowired
    public AdSetService(WordstreamAdSetDAO wordstreamAdSetDAO, GeoLocationTransformer geoLocationTransformer) {
        this.wordstreamAdSetDAO = wordstreamAdSetDAO;
        this.geoLocationTransformer = geoLocationTransformer;
    }

    /**
     * Fetch the Wordstream data and return the top geo locations.
     */
    public List<TopGeoLocation> findTopGeoLocations(Integer limit) throws WordstreamAdSetDataException {
        if (limit == null || limit < 1) {
            throw new IllegalArgumentException("Illegal value for parameter 'limit': " + limit);
        }

        // fetch the ad set data from the datasource
        AdSetListWrapper adSetData = wordstreamAdSetDAO.fetchAdSetData();

        return findTopXGeoLocations(adSetData.getAdSetData(), limit);
    }

    /**
     * Given the Facebook AdSet data parsed from the data source, find the top geo locations.
     */
    List<TopGeoLocation> findTopXGeoLocations(List<AdSet> adSetData, int limit) {
        if (limit < 1) {
            return Collections.emptyList();
        }

        Map<GeoLocation, Integer> geoLocationCounts = new HashMap<>();

        // get the count for each geo location in the dataset
        for (AdSet adSet : adSetData) {
            Targeting targeting = adSet.getFieldTargeting();

            if (targeting != null) {
                TargetingGeoLocation targetingGeoLocation = targeting.getFieldGeoLocations();

                // convert to the local representation of a geo location to take advantage of an actual hashcode method
                GeoLocation geoLocation = geoLocationTransformer.toGeoLocation(targetingGeoLocation);

                if (geoLocation != null) {
                    int currentCount = geoLocationCounts.getOrDefault(geoLocation, 0);
                    geoLocationCounts.put(geoLocation, currentCount + 1);
                }
            }
        }

        // convert the map of GeoLocation:count pairs to a list of TopGeoLocation, ordered by count desc
        return geoLocationCounts.entrySet()
                .stream()
                // order the map by value (desc)
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .collect(Collectors.toList())
                // isolate the top values
                .subList(0, Math.min(limit, geoLocationCounts.size()))
                .stream()
                .map(entry -> new TopGeoLocation(entry.getKey(), entry.getValue()))
                .collect(Collectors.toList());
    }
}
