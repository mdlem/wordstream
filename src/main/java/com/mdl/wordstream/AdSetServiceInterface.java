package com.mdl.wordstream;

import com.mdl.wordstream.dto.TopGeoLocation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * Define the service interface for fetching the top geo locations.
 */
public interface AdSetServiceInterface {
    @GetMapping("/adset/geolocations/top")
    List<TopGeoLocation> getMostFrequentGeoLocations(@RequestParam(value = "limit", defaultValue = "5") Integer limit) throws Exception;
}
