package com.mdl.wordstream.dto;

import com.google.gson.annotations.SerializedName;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
public class TopGeoLocation {
    @SerializedName("geoLocation")
    private GeoLocation geoLocation;
    @SerializedName("count")
    private Integer count;
}
