package com.mdl.wordstream.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.gson.annotations.SerializedName;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
@EqualsAndHashCode
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GeoLocationMarket {
    @SerializedName("country")
    private String country;
    @SerializedName("key")
    private String key;
    @SerializedName("name")
    private String name;
    @SerializedName("market_type")
    private String marketType;
}
