package com.mdl.wordstream.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.gson.annotations.SerializedName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@EqualsAndHashCode
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GeoLocation {
    @SerializedName("cities")
    private List<GeoLocationCity> cities;
    @SerializedName("countries")
    private List<String> countries;
    @SerializedName("country_groups")
    private List<String> countryGroups;
    @SerializedName("custom_locations")
    private List<GeoLocationCustomLocation> customLocations;
    @SerializedName("electoral_districts")
    private List<GeoLocationElectoralDistrict> electoralDistricts;
    @SerializedName("geo_markets")
    private List<GeoLocationMarket> geoMarkets;
    @SerializedName("large_geo_areas")
    private List<GeoLocationGeoEntities> largeGeoAreas;
    @SerializedName("location_cluster_ids")
    private List<GeoLocationLocationCluster> locationClusterIds;
    @SerializedName("location_expansion")
    private GeoLocationLocationExpansion locationExpansion;
    @SerializedName("location_set_ids")
    private List<String> locationSetIds;
    @SerializedName("location_types")
    private List<String> locationTypes;
    @SerializedName("medium_geo_areas")
    private List<GeoLocationGeoEntities> mediumGeoAreas;
    @SerializedName("metro_areas")
    private List<GeoLocationGeoEntities> metroAreas;
    @SerializedName("neighborhoods")
    private List<GeoLocationGeoEntities> neighborhoods;
    @SerializedName("places")
    private List<GeoLocationPlace> places;
    @SerializedName("political_districts")
    private List<GeoLocationPoliticalDistrict> politicalDistricts;
    @SerializedName("regions")
    private List<GeoLocationRegion> regions;
    @SerializedName("small_geo_areas")
    private List<GeoLocationGeoEntities> smallGeoAreas;
    @SerializedName("subcities")
    private List<GeoLocationGeoEntities> subcities;
    @SerializedName("subneighborhoods")
    private List<GeoLocationGeoEntities> subneighborhoods;
    @SerializedName("zips")
    private List<GeoLocationZip> zips;
}
