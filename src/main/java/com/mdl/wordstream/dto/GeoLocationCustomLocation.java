package com.mdl.wordstream.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.gson.annotations.SerializedName;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
@EqualsAndHashCode
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GeoLocationCustomLocation {
    @SerializedName("country")
    private String country;
    @SerializedName("key")
    private String key;
    @SerializedName("name")
    private String name;
    @SerializedName("address_string")
    private String addressString;
    @SerializedName("country_group")
    private String countryGroup;
    @SerializedName("custom_type")
    private String customType;
    @SerializedName("distance_unit")
    private String distanceUnit;
    @SerializedName("latitude")
    private Double latitude;
    @SerializedName("longitude")
    private Double longitude;
    @SerializedName("max_population")
    private Long maxPopulation;
    @SerializedName("min_population")
    private Long minPopulation;
    @SerializedName("primary_city_id")
    private Long primaryCityId;
    @SerializedName("radius")
    private Double radius;
    @SerializedName("region_id")
    private Long regionId;
}
