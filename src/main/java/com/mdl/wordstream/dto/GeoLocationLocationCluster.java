package com.mdl.wordstream.dto;

import com.google.gson.annotations.SerializedName;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
@EqualsAndHashCode
public class GeoLocationLocationCluster {
    @SerializedName("key")
    private Long key;
}
