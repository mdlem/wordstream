package com.mdl.wordstream.model;

import com.facebook.ads.sdk.AdSet;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class AdSetListWrapper {
    @SerializedName("data")
    private List<AdSet> adSetData;
}
