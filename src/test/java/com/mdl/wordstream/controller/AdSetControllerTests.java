package com.mdl.wordstream.controller;

import com.mdl.wordstream.dto.GeoLocation;
import com.mdl.wordstream.dto.GeoLocationCity;
import com.mdl.wordstream.dto.GeoLocationRegion;
import com.mdl.wordstream.dto.GeoLocationZip;
import com.mdl.wordstream.dto.TopGeoLocation;
import com.mdl.wordstream.exception.WordstreamAdSetDataException;
import com.mdl.wordstream.service.AdSetService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest
public class AdSetControllerTests {

    private static final String PATH = "/adset/geolocations/top";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private AdSetService mockAdSetService;

    @Test
    public void validResponse() throws Exception {
        String expectedJson = "[\n" +
                "  {\n" +
                "    \"geoLocation\": {\n" +
                "      \"regions\": [\n" +
                "        {\n" +
                "          \"country\": \"US\",\n" +
                "          \"key\": \"1\",\n" +
                "          \"name\": \"MA\"\n" +
                "        }\n" +
                "      ],\n" +
                "      \"cities\": [\n" +
                "        {\n" +
                "          \"country\": \"US\",\n" +
                "          \"key\": \"1\",\n" +
                "          \"name\": \"Boston\",\n" +
                "          \"distanceUnit\": \"mile\",\n" +
                "          \"region\": \"MA\",\n" +
                "          \"regionId\": \"1\"\n" +
                "        }\n" +
                "      ]\n" +
                "    },\n" +
                "    \"count\": 5\n" +
                "  },\n" +
                "  {\n" +
                "    \"geoLocation\": {\n" +
                "      \"regions\": [\n" +
                "        {\n" +
                "          \"country\": \"US\",\n" +
                "          \"key\": \"2\",\n" +
                "          \"name\": \"RI\"\n" +
                "        }\n" +
                "      ],\n" +
                "      \"cities\": [\n" +
                "        {\n" +
                "          \"country\": \"US\",\n" +
                "          \"key\": \"2\",\n" +
                "          \"name\": \"Providence\",\n" +
                "          \"distanceUnit\": \"mile\",\n" +
                "          \"region\": \"RI\",\n" +
                "          \"regionId\": \"2\"\n" +
                "        }\n" +
                "      ]\n" +
                "    },\n" +
                "    \"count\": 3\n" +
                "  },\n" +
                "  {\n" +
                "    \"geoLocation\": {\n" +
                "      \"regions\": [\n" +
                "        {\n" +
                "          \"country\": \"US\",\n" +
                "          \"key\": \"1\",\n" +
                "          \"name\": \"MA\"\n" +
                "        }\n" +
                "      ]\n" +
                "    },\n" +
                "    \"count\": 2\n" +
                "  },\n" +
                "  {\n" +
                "    \"geoLocation\": {\n" +
                "      \"regions\": [\n" +
                "        {\n" +
                "          \"country\": \"US\",\n" +
                "          \"key\": \"1\",\n" +
                "          \"name\": \"MA\"\n" +
                "        },\n" +
                "        {\n" +
                "          \"country\": \"US\",\n" +
                "          \"key\": \"2\",\n" +
                "          \"name\": \"RI\"\n" +
                "        }\n" +
                "      ]\n" +
                "    },\n" +
                "    \"count\": 2\n" +
                "  },\n" +
                "  {\n" +
                "    \"geoLocation\": {\n" +
                "      \"cities\": [\n" +
                "        {\n" +
                "          \"country\": \"US\",\n" +
                "          \"key\": \"1\",\n" +
                "          \"name\": \"Boston\",\n" +
                "          \"distanceUnit\": \"mile\",\n" +
                "          \"region\": \"MA\",\n" +
                "          \"regionId\": \"1\"\n" +
                "        },\n" +
                "        {\n" +
                "          \"country\": \"US\",\n" +
                "          \"key\": \"2\",\n" +
                "          \"name\": \"Providence\",\n" +
                "          \"distanceUnit\": \"mile\",\n" +
                "          \"region\": \"RI\",\n" +
                "          \"regionId\": \"2\"\n" +
                "        }\n" +
                "      ]\n" +
                "    },\n" +
                "    \"count\": 2\n" +
                "  }\n" +
                "]";

        when(mockAdSetService.findTopGeoLocations(5))
                .thenReturn(createTopGeoLocations(5));

        this.mockMvc.perform(get(PATH))
                .andExpect(status().isOk())
                .andExpect(content().json(expectedJson));
    }

    @Test
    public void limitedResponse() throws Exception {
        String expectedJson = "[\n" +
                "  {\n" +
                "    \"geoLocation\": {\n" +
                "      \"regions\": [\n" +
                "        {\n" +
                "          \"country\": \"US\",\n" +
                "          \"key\": \"1\",\n" +
                "          \"name\": \"MA\"\n" +
                "        }\n" +
                "      ],\n" +
                "      \"cities\": [\n" +
                "        {\n" +
                "          \"country\": \"US\",\n" +
                "          \"key\": \"1\",\n" +
                "          \"name\": \"Boston\",\n" +
                "          \"distanceUnit\": \"mile\",\n" +
                "          \"region\": \"MA\",\n" +
                "          \"regionId\": \"1\"\n" +
                "        }\n" +
                "      ]\n" +
                "    },\n" +
                "    \"count\": 5\n" +
                "  },\n" +
                "  {\n" +
                "    \"geoLocation\": {\n" +
                "      \"regions\": [\n" +
                "        {\n" +
                "          \"country\": \"US\",\n" +
                "          \"key\": \"2\",\n" +
                "          \"name\": \"RI\"\n" +
                "        }\n" +
                "      ],\n" +
                "      \"cities\": [\n" +
                "        {\n" +
                "          \"country\": \"US\",\n" +
                "          \"key\": \"2\",\n" +
                "          \"name\": \"Providence\",\n" +
                "          \"distanceUnit\": \"mile\",\n" +
                "          \"region\": \"RI\",\n" +
                "          \"regionId\": \"2\"\n" +
                "        }\n" +
                "      ]\n" +
                "    },\n" +
                "    \"count\": 3\n" +
                "  }\n" +
                "]";

        when(mockAdSetService.findTopGeoLocations(5))
                .thenReturn(createTopGeoLocations(2));

        this.mockMvc.perform(get(PATH))
                .andExpect(status().isOk())
                .andExpect(content().json(expectedJson));
    }

    @Test
    public void invalidPath() throws Exception {
        this.mockMvc.perform(get("/some/fake/path/"))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void invalidLimit() throws Exception {
        this.mockMvc.perform(get(PATH + "?limit=abc"))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void unknownParam() throws Exception {
        this.mockMvc.perform(get(PATH + "?somerandomparam=abc"))
                .andExpect(status().isOk());
    }

    private List<TopGeoLocation> createTopGeoLocations(int limit) {
        GeoLocationRegion region1 = GeoLocationRegion.builder()
                .country("US")
                .key("1")
                .name("MA")
                .build();

        GeoLocationRegion region2 = GeoLocationRegion.builder()
                .country("US")
                .key("2")
                .name("RI")
                .build();

        GeoLocationCity city1 = GeoLocationCity.builder()
                .country("US")
                .distanceUnit("mile")
                .key("1")
                .radius(10L)
                .name("Boston")
                .region("MA")
                .regionId("1")
                .build();

        GeoLocationCity city2 = GeoLocationCity.builder()
                .country("US")
                .distanceUnit("mile")
                .key("2")
                .radius(20L)
                .name("Providence")
                .region("RI")
                .regionId("2")
                .build();

        GeoLocationZip zip1 = GeoLocationZip.builder()
                .country("US")
                .primaryCityId(1L)
                .name("00000")
                .key("US:00000")
                .regionId(1L)
                .build();

        GeoLocation geoLocation1 = GeoLocation.builder()
                .regions(Collections.singletonList(region1))
                .cities(Collections.singletonList(city1))
                .build();

        GeoLocation geoLocation2 = GeoLocation.builder()
                .regions(Collections.singletonList(region2))
                .cities(Collections.singletonList(city2))
                .build();

        GeoLocation geoLocation3 = GeoLocation.builder()
                .regions(Collections.singletonList(region1))
                .build();

        GeoLocation geoLocation4 = GeoLocation.builder()
                .regions(Arrays.asList(region1, region2))
                .build();

        GeoLocation geoLocation5 = GeoLocation.builder()
                .cities(Arrays.asList(city1, city2))
                .build();

        GeoLocation geoLocation6 = GeoLocation.builder()
                .zips(Collections.singletonList(zip1))
                .build();

        TopGeoLocation topGeoLocation1 = new TopGeoLocation(geoLocation1, 5);
        TopGeoLocation topGeoLocation2 = new TopGeoLocation(geoLocation2, 3);
        TopGeoLocation topGeoLocation3 = new TopGeoLocation(geoLocation3, 2);
        TopGeoLocation topGeoLocation4 = new TopGeoLocation(geoLocation4, 2);
        TopGeoLocation topGeoLocation5 = new TopGeoLocation(geoLocation5, 2);
        TopGeoLocation topGeoLocation6 = new TopGeoLocation(geoLocation6, 1);
        return Arrays.asList(topGeoLocation1, topGeoLocation2, topGeoLocation3, topGeoLocation4, topGeoLocation5, topGeoLocation6)
                .subList(0, limit);
    }
}
