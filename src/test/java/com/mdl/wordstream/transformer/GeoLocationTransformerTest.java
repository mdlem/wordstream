package com.mdl.wordstream.transformer;

import com.facebook.ads.sdk.TargetingGeoLocation;
import com.facebook.ads.sdk.TargetingGeoLocationCity;
import com.facebook.ads.sdk.TargetingGeoLocationMarket;
import com.facebook.ads.sdk.TargetingGeoLocationRegion;
import com.facebook.ads.sdk.TargetingGeoLocationZip;
import com.mdl.wordstream.dto.GeoLocation;
import com.mdl.wordstream.dto.GeoLocationCity;
import com.mdl.wordstream.dto.GeoLocationMarket;
import com.mdl.wordstream.dto.GeoLocationRegion;
import com.mdl.wordstream.dto.GeoLocationZip;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class GeoLocationTransformerTest {

    private GeoLocationTransformer geoLocationTransformer;

    @Before
    public void setUp() {
        geoLocationTransformer = new GeoLocationTransformer();
    }

    @Test
    public void transformGeoLocationList() {
        TargetingGeoLocation targetingGeoLocation = createTargetingGeoLocations();
        GeoLocation expected = createExpectedGeoLocation();

        GeoLocation actual = geoLocationTransformer.toGeoLocation(targetingGeoLocation);
        assertEquals(expected, actual);
    }

    private GeoLocation createExpectedGeoLocation() {
        GeoLocationRegion region = GeoLocationRegion.builder()
                .country("rcountry")
                .name("rname")
                .key("rkey")
                .build();

        List<String> countries = Arrays.asList("a", "b");

        List<String> locationTypes = Arrays.asList("z", "y");

        GeoLocationZip zip = GeoLocationZip.builder()
                .country("zcountry")
                .key("zkey")
                .name("zname")
                .regionId(234L)
                .primaryCityId(12345L)
                .build();

        GeoLocationMarket market = GeoLocationMarket.builder()
                .country("mcountry")
                .key("mkey")
                .name("mname")
                .marketType("mmarkettype")
                .build();

        GeoLocationCity city = GeoLocationCity.builder()
                .country("ccountry")
                .distanceUnit("cunit")
                .key("ckey")
                .name("cname")
                .radius(100L)
                .region("cregion")
                .regionId("cregionid")
                .build();

        return GeoLocation.builder()
                .regions(Collections.singletonList(region))
                .geoMarkets(Collections.singletonList(market))
                .locationTypes(locationTypes)
                .countries(countries)
                .zips(Collections.singletonList(zip))
                .cities(Collections.singletonList(city))
                .build();
    }

    private TargetingGeoLocation createTargetingGeoLocations() {
        TargetingGeoLocationRegion region = new TargetingGeoLocationRegion()
                .setFieldName("rname")
                .setFieldKey("rkey")
                .setFieldCountry("rcountry");

        List<String> countries = Arrays.asList("a", "b");

        List<String> locationTypes = Arrays.asList("z", "y");

        TargetingGeoLocationZip zip = new TargetingGeoLocationZip()
                .setFieldCountry("zcountry")
                .setFieldKey("zkey")
                .setFieldName("zname")
                .setFieldRegionId(234L)
                .setFieldPrimaryCityId(12345L);

        TargetingGeoLocationMarket market = new TargetingGeoLocationMarket()
                .setFieldCountry("mcountry")
                .setFieldKey("mkey")
                .setFieldName("mname")
                .setFieldMarketType("mmarkettype");

        TargetingGeoLocationCity city = new TargetingGeoLocationCity()
                .setFieldCountry("ccountry")
                .setFieldDistanceUnit("cunit")
                .setFieldKey("ckey")
                .setFieldName("cname")
                .setFieldRadius(100L)
                .setFieldRegion("cregion")
                .setFieldRegionId("cregionid");

        return new TargetingGeoLocation()
                .setFieldRegions(Collections.singletonList(region))
                .setFieldGeoMarkets(Collections.singletonList(market))
                .setFieldLocationTypes(locationTypes)
                .setFieldCountries(countries)
                .setFieldZips(Collections.singletonList(zip))
                .setFieldCities(Collections.singletonList(city));
    }
}
