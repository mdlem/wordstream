package com.mdl.wordstream;

import com.facebook.ads.sdk.APINode;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class JsonListEqualityChecker {

    private static JsonListEqualityChecker INSTANCE = null;

    private final Gson serializer = new Gson();
    private final JsonParser parser = new JsonParser();

    private JsonListEqualityChecker() { }

    public static JsonListEqualityChecker getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new JsonListEqualityChecker();
        }

        return INSTANCE;
    }

    // the facebook sdk doesn't provide an "equals" operator, so let's compare the serialized objects
    public void assertEqualLists(List<? extends APINode> expected, List<? extends APINode> actual) {
        JsonElement expectedJson = parser.parse(serializer.toJson(expected));
        JsonElement actualJson = parser.parse(serializer.toJson(actual));
        assertEquals(actualJson, expectedJson);
    }
}
