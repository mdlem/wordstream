package com.mdl.wordstream.service;

import com.facebook.ads.sdk.AdSet;
import com.facebook.ads.sdk.FlexibleTargeting;
import com.facebook.ads.sdk.IDName;
import com.facebook.ads.sdk.Targeting;
import com.facebook.ads.sdk.TargetingGeoLocation;
import com.facebook.ads.sdk.TargetingGeoLocationCity;
import com.facebook.ads.sdk.TargetingGeoLocationRegion;
import com.mdl.wordstream.dao.WordstreamAdSetDAO;
import com.mdl.wordstream.dto.GeoLocation;
import com.mdl.wordstream.dto.GeoLocationCity;
import com.mdl.wordstream.dto.GeoLocationRegion;
import com.mdl.wordstream.dto.TopGeoLocation;
import com.mdl.wordstream.exception.WordstreamAdSetDataException;
import com.mdl.wordstream.transformer.GeoLocationTransformer;
import org.assertj.core.api.SoftAssertions;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class AdSetServiceTests {

    private SoftAssertions softly;

    private AdSetService adSetService;
    private WordstreamAdSetDAO mockWordstreamAdSetDAO;

    private TargetingGeoLocationRegion targetingRegion1;
    private TargetingGeoLocationRegion targetingRegion2;
    private TargetingGeoLocationCity targetingCity1;
    private TargetingGeoLocationCity targetingCity2;
    private GeoLocationRegion region1;
    private GeoLocationRegion region2;
    private GeoLocationCity city1;
    private GeoLocationCity city2;
    private String locationType1;
    private String locationType2;

    @Before
    public void setUp() {
        softly = new SoftAssertions();

        mockWordstreamAdSetDAO = mock(WordstreamAdSetDAO.class);
        GeoLocationTransformer geoLocationTransformer = new GeoLocationTransformer();
        adSetService = new AdSetService(mockWordstreamAdSetDAO, geoLocationTransformer);

        initializeTestGeoLocationData();
    }

    @Test
    public void noGeoLocations() {
        List<AdSet> adSetDataNoGeoLocations = Collections.singletonList(createAdSetNoGeoLocation());
        List<TopGeoLocation> actual = adSetService.findTopXGeoLocations(adSetDataNoGeoLocations, 1);
        assertTrue(actual.isEmpty());
    }

    @Test
    public void singleGeoLocation() {
        List<GeoLocation> expected = Collections.singletonList(createGeoLocation(region1, null, null, null, locationType1, null));

        List<AdSet> adSetData = createAdSetDataSingleRegion();
        List<TopGeoLocation> actual = adSetService.findTopXGeoLocations(adSetData, 1);

        List<GeoLocation> actualGeoLocations = actual.stream().map(TopGeoLocation::getGeoLocation).collect(Collectors.toList());
        assertEquals(expected, actualGeoLocations);
    }

    @Test
    public void multipleGeoLocations() {
        GeoLocation geoLocation1 = createGeoLocation(region1, null, null, null, locationType1, null);
        GeoLocation geoLocation2 = createGeoLocation(null, region2, null, null, locationType1, null);
        GeoLocation geoLocation3 = createGeoLocation(null, null, city1, null, locationType1, null);
        GeoLocation geoLocation4 = createGeoLocation(null, null, null, city2, locationType1, null);
        List<GeoLocation> expected = Arrays.asList(geoLocation1, geoLocation2, geoLocation3, geoLocation4);

        List<AdSet> adSetData = createAdSetDataMultipleGeoLocations();

        // the data set contains 4 distinct geo locations and we ask for the top 5 - expect only 4 results
        List<TopGeoLocation> actual = adSetService.findTopXGeoLocations(adSetData, 5);

        softly.assertThat(actual.get(0).getCount()).isEqualTo(4);
        softly.assertThat(actual.get(1).getCount()).isEqualTo(2);
        softly.assertThat(actual.get(2).getCount()).isEqualTo(2);
        softly.assertThat(actual.get(3).getCount()).isEqualTo(1);

        List<GeoLocation> actualGeoLocations = actual.stream().map(TopGeoLocation::getGeoLocation).collect(Collectors.toList());
        assertEquals(expected, actualGeoLocations);
    }

    @Test
    public void topMultipleGeoLocations() {
        GeoLocation geoLocation1 = createGeoLocation(region1, null, null, null, locationType1, null);
        GeoLocation geoLocation2 = createGeoLocation(null, region2, null, null, locationType1, null);
        GeoLocation geoLocation3 = createGeoLocation(null, null, city1, null, locationType1, null);
        List<GeoLocation> expected = Arrays.asList(geoLocation1, geoLocation2, geoLocation3);

        // more than 3 possible geo locations in the data set, but only the top 3 will be displayed
        List<AdSet> adSetData = createAdSetDataMultipleGeoLocations();
        List<TopGeoLocation> actual = adSetService.findTopXGeoLocations(adSetData, 3);

        softly.assertThat(actual.get(0).getCount()).isEqualTo(4);
        softly.assertThat(actual.get(1).getCount()).isEqualTo(2);
        softly.assertThat(actual.get(2).getCount()).isEqualTo(2);

        List<GeoLocation> actualGeoLocations = actual.stream().map(TopGeoLocation::getGeoLocation).collect(Collectors.toList());
        assertEquals(expected, actualGeoLocations);
    }

    @Test
    public void multipleMultivalueGeoLocations() {
        GeoLocation geoLocation1 = createGeoLocation(region1, null, null, null, locationType1, null);
        GeoLocation geoLocation2 = createGeoLocation(null, region2, null, null, locationType1, null);
        GeoLocation geoLocation3 = createGeoLocation(region1, null, city1, null, locationType1, null);
        List<GeoLocation> expected = Arrays.asList(geoLocation1, geoLocation2, geoLocation3);

        // more than 3 possible geo locations in the data set, but only the top 3 will be displayed
        List<AdSet> adSetData = createAdSetDataMultipleGeoLocationsWithMultipleValues();
        List<TopGeoLocation> actual = adSetService.findTopXGeoLocations(adSetData, 3);

        softly.assertThat(actual.get(0).getCount()).isEqualTo(3);
        softly.assertThat(actual.get(1).getCount()).isEqualTo(2);
        softly.assertThat(actual.get(2).getCount()).isEqualTo(2);

        List<GeoLocation> actualGeoLocations = actual.stream().map(TopGeoLocation::getGeoLocation).collect(Collectors.toList());
        assertEquals(expected, actualGeoLocations);
    }

    @Test(expected = WordstreamAdSetDataException.class)
    public void parseFailed() throws WordstreamAdSetDataException {
        when(mockWordstreamAdSetDAO.fetchAdSetData()).thenThrow(WordstreamAdSetDataException.class);
        adSetService.findTopGeoLocations(5);
    }

    @Test(expected = IllegalArgumentException.class)
    public void illegalTopValue() throws WordstreamAdSetDataException {
        adSetService.findTopGeoLocations(-1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void nullTopValue() throws WordstreamAdSetDataException {
        adSetService.findTopGeoLocations(null);
    }


    private AdSet createAdSetNoGeoLocation() {
        IDName idName = new IDName()
                .setFieldId("6003346088127")
                .setFieldName("Fitchburg State University");
        List<IDName> interests = Collections.singletonList(idName);
        List<FlexibleTargeting> flexibleTargetings = Collections.singletonList(new FlexibleTargeting().setFieldInterests(interests));

        Targeting targeting = new Targeting()
                .setFieldFlexibleSpec(flexibleTargetings)
                .setFieldPublisherPlatforms(Arrays.asList("facebook", "instagram", "audience_network", "messenger"))
                .setFieldAudienceNetworkPositions(Collections.singletonList("classic"))
                .setFieldInstagramPositions(Collections.singletonList("stream"))
                .setFieldAgeMin(35L)
                .setFieldAgeMax(60L)
                .setFieldDevicePlatforms(Arrays.asList(Targeting.EnumDevicePlatforms.VALUE_MOBILE, Targeting.EnumDevicePlatforms.VALUE_DESKTOP))
                .setFieldFacebookPositions(Arrays.asList("feed", "right_hand_column", "instant_article", "marketplace"))
                .setFieldMessengerPositions(Collections.singletonList("messenger_home"));

        return new AdSet()
                .setFieldTargeting(targeting)
                .setFieldId("6095747941990");
    }

    private List<AdSet> createAdSetDataSingleRegion() {
        TargetingGeoLocation geoLocation = new TargetingGeoLocation()
                .setFieldRegions(Collections.singletonList(targetingRegion1))
                .setFieldLocationTypes(Collections.singletonList(locationType1));

        AdSet adSet = createAdSetNoGeoLocation();
        adSet.getFieldTargeting().setFieldGeoLocations(geoLocation);

        List<AdSet> adSetData = new ArrayList<>();
        adSetData.add(adSet);
        return adSetData;
    }

    private List<AdSet> createAdSetDataMultipleGeoLocations() {
        TargetingGeoLocation geoLocation1 = new TargetingGeoLocation()
                .setFieldRegions(Collections.singletonList(targetingRegion1))
                .setFieldLocationTypes(Collections.singletonList(locationType1));
        TargetingGeoLocation geoLocation2 = new TargetingGeoLocation()
                .setFieldRegions(Collections.singletonList(targetingRegion2))
                .setFieldLocationTypes(Collections.singletonList(locationType1));
        TargetingGeoLocation geoLocation3 = new TargetingGeoLocation()
                .setFieldCities(Collections.singletonList(targetingCity1))
                .setFieldLocationTypes(Collections.singletonList(locationType1));
        TargetingGeoLocation geoLocation4 = new TargetingGeoLocation()
                .setFieldCities(Collections.singletonList(targetingCity2))
                .setFieldLocationTypes(Collections.singletonList(locationType1));

        AdSet adSet1 = createAdSetNoGeoLocation();
        AdSet adSet2 = createAdSetNoGeoLocation();
        AdSet adSet3 = createAdSetNoGeoLocation();
        AdSet adSet4 = createAdSetNoGeoLocation();
        AdSet adSet5 = createAdSetNoGeoLocation();
        AdSet adSet6 = createAdSetNoGeoLocation();
        AdSet adSet7 = createAdSetNoGeoLocation();
        AdSet adSet8 = createAdSetNoGeoLocation();
        AdSet adSet9 = createAdSetNoGeoLocation();
        AdSet adSet10 = createAdSetNoGeoLocation();

        // add the same geo locations multiple times to cause some to be used more frequently than others
        adSet1.getFieldTargeting().setFieldGeoLocations(geoLocation1);
        adSet2.getFieldTargeting().setFieldGeoLocations(geoLocation1);
        adSet3.getFieldTargeting().setFieldGeoLocations(geoLocation1);
        adSet4.getFieldTargeting().setFieldGeoLocations(geoLocation1);
        adSet5.getFieldTargeting().setFieldGeoLocations(geoLocation2);
        adSet6.getFieldTargeting().setFieldGeoLocations(geoLocation2);
        adSet7.getFieldTargeting().setFieldGeoLocations(geoLocation2);
        adSet8.getFieldTargeting().setFieldGeoLocations(geoLocation3);
        adSet9.getFieldTargeting().setFieldGeoLocations(geoLocation3);
        adSet10.getFieldTargeting().setFieldGeoLocations(geoLocation4);

        return Arrays.asList(adSet1, adSet2, adSet3, adSet4, adSet5, adSet6, adSet7, adSet8, adSet9, adSet10);
    }

    private List<AdSet> createAdSetDataMultipleGeoLocationsWithMultipleValues() {
        TargetingGeoLocation geoLocation1 = new TargetingGeoLocation()
                .setFieldRegions(Collections.singletonList(targetingRegion1))
                .setFieldLocationTypes(Collections.singletonList(locationType1));
        TargetingGeoLocation geoLocation2 = new TargetingGeoLocation()
                .setFieldRegions(Collections.singletonList(targetingRegion2))
                .setFieldLocationTypes(Collections.singletonList(locationType1));
        TargetingGeoLocation geoLocation3 = new TargetingGeoLocation()
                .setFieldRegions(Collections.singletonList(targetingRegion1))
                .setFieldCities(Collections.singletonList(targetingCity1))
                .setFieldLocationTypes(Collections.singletonList(locationType1));
        TargetingGeoLocation geoLocation4 = new TargetingGeoLocation()
                .setFieldRegions(Collections.singletonList(targetingRegion1))
                .setFieldCities(Arrays.asList(targetingCity1, targetingCity2))
                .setFieldLocationTypes(Collections.singletonList(locationType1));
        TargetingGeoLocation geoLocation5 = new TargetingGeoLocation()
                .setFieldRegions(Arrays.asList(targetingRegion1, targetingRegion2))
                .setFieldCities(Arrays.asList(targetingCity1, targetingCity2))
                .setFieldLocationTypes(Arrays.asList(locationType1, locationType2));

        AdSet adSet1 = createAdSetNoGeoLocation();
        AdSet adSet2 = createAdSetNoGeoLocation();
        AdSet adSet3 = createAdSetNoGeoLocation();
        AdSet adSet4 = createAdSetNoGeoLocation();
        AdSet adSet5 = createAdSetNoGeoLocation();
        AdSet adSet6 = createAdSetNoGeoLocation();
        AdSet adSet7 = createAdSetNoGeoLocation();
        AdSet adSet8 = createAdSetNoGeoLocation();
        AdSet adSet9 = createAdSetNoGeoLocation();
        AdSet adSet10 = createAdSetNoGeoLocation();
        AdSet adSet11 = createAdSetNoGeoLocation();

        // add the same geo locations multiple times to cause some to be used more frequently than others
        adSet1.getFieldTargeting().setFieldGeoLocations(geoLocation1);
        adSet2.getFieldTargeting().setFieldGeoLocations(geoLocation1);
        adSet3.getFieldTargeting().setFieldGeoLocations(geoLocation1);
        adSet4.getFieldTargeting().setFieldGeoLocations(geoLocation1);
        adSet5.getFieldTargeting().setFieldGeoLocations(geoLocation2);
        adSet6.getFieldTargeting().setFieldGeoLocations(geoLocation2);
        adSet7.getFieldTargeting().setFieldGeoLocations(geoLocation2);
        adSet8.getFieldTargeting().setFieldGeoLocations(geoLocation3);
        adSet9.getFieldTargeting().setFieldGeoLocations(geoLocation3);
        adSet10.getFieldTargeting().setFieldGeoLocations(geoLocation4);
        adSet11.getFieldTargeting().setFieldGeoLocations(geoLocation5);

        return Arrays.asList(adSet1, adSet2, adSet3, adSet4, adSet5, adSet6, adSet7, adSet8, adSet9, adSet10, adSet11);
    }

    private GeoLocation createGeoLocation(GeoLocationRegion region1, GeoLocationRegion region2, GeoLocationCity city1, GeoLocationCity city2, String locationType1, String locationType2) {
        GeoLocation geoLocation = new GeoLocation();

        if (region1 != null) {
            List<GeoLocationRegion> current = geoLocation.getRegions();
            if (current == null) {
                current = new ArrayList<>();
            }
            current.add(region1);
            geoLocation.setRegions(current);
        }

        if (region2 != null) {
            List<GeoLocationRegion> current = geoLocation.getRegions();
            if (current == null) {
                current = new ArrayList<>();
            }
            current.add(region2);
            geoLocation.setRegions(current);
        }

        if (city1 != null) {
            List<GeoLocationCity> current = geoLocation.getCities();
            if (current == null) {
                current = new ArrayList<>();
            }
            current.add(city1);
            geoLocation.setCities(current);
        }

        if (city2 != null) {
            List<GeoLocationCity> current = geoLocation.getCities();
            if (current == null) {
                current = new ArrayList<>();
            }
            current.add(city2);
            geoLocation.setCities(current);
        }

        if (locationType1 != null) {
            List<String> current = geoLocation.getLocationTypes();
            if (current == null) {
                current = new ArrayList<>();
            }
            current.add(locationType1);
            geoLocation.setLocationTypes(current);
        }

        if (locationType2 != null) {
            List<String> current = geoLocation.getLocationTypes();
            if (current == null) {
                current = new ArrayList<>();
            }
            current.add(locationType2);
            geoLocation.setLocationTypes(current);
        }

        return geoLocation;
    }

    private void initializeTestGeoLocationData() {
        targetingRegion1 = new TargetingGeoLocationRegion()
                .setFieldCountry("US")
                .setFieldKey("3864")
                .setFieldName("Massachusetts");

        targetingRegion2 = new TargetingGeoLocationRegion()
                .setFieldCountry("US")
                .setFieldKey("3882")
                .setFieldName("Rhode Island");

        targetingCity1 = new TargetingGeoLocationCity()
                .setFieldCountry("US")
                .setFieldDistanceUnit("mile")
                .setFieldKey("111111")
                .setFieldRadius(50L)
                .setFieldName("Boston")
                .setFieldRegion("Massachusetts")
                .setFieldRegionId("3864");

        targetingCity2 = new TargetingGeoLocationCity()
                .setFieldCountry("US")
                .setFieldDistanceUnit("mile")
                .setFieldKey("222222")
                .setFieldRadius(50L)
                .setFieldName("Providence")
                .setFieldRegion("Rhode Island")
                .setFieldRegionId("3882");

        region1 = GeoLocationRegion.builder()
                .country("US")
                .key("3864")
                .name("Massachusetts")
                .build();

        region2 = GeoLocationRegion.builder()
                .country("US")
                .key("3882")
                .name("Rhode Island")
                .build();

        city1 = GeoLocationCity.builder()
                .country("US")
                .distanceUnit("mile")
                .key("111111")
                .radius(50L)
                .name("Boston")
                .region("Massachusetts")
                .regionId("3864")
                .build();

        city2 = GeoLocationCity.builder()
                .country("US")
                .distanceUnit("mile")
                .key("222222")
                .radius(50L)
                .name("Providence")
                .region("Rhode Island")
                .regionId("3882")
                .build();

        locationType1 = "home";
        locationType2 = "recent";
    }
}
