package com.mdl.wordstream.dao;

import com.facebook.ads.sdk.AdSet;
import com.facebook.ads.sdk.FlexibleTargeting;
import com.facebook.ads.sdk.IDName;
import com.facebook.ads.sdk.Targeting;
import com.facebook.ads.sdk.TargetingGeoLocation;
import com.facebook.ads.sdk.TargetingGeoLocationRegion;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.mdl.wordstream.JsonListEqualityChecker;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class WordstreamAdSetDAOTests {

    private WordstreamAdSetDAO wordstreamAdSetDAO;

    @Before
    public void setUp() {
        wordstreamAdSetDAO = new WordstreamAdSetDAO();
    }

    @Test
    public void parseSimpleAdSet() {
        String data = "{\n" +
                "  \"data\": [\n" +
                "    {\n" +
                "      \"targeting\": {\n" +
                "        \"flexible_spec\": [\n" +
                "          {\n" +
                "            \"interests\": [\n" +
                "              {\n" +
                "                \"id\": \"6003346088127\",\n" +
                "                \"name\": \"Fitchburg State University\"\n" +
                "              }\n" +
                "            ]\n" +
                "          }\n" +
                "        ],\n" +
                "        \"publisher_platforms\": [\n" +
                "          \"facebook\",\n" +
                "          \"instagram\",\n" +
                "          \"audience_network\",\n" +
                "          \"messenger\"\n" +
                "        ],\n" +
                "        \"geo_locations\": {\n" +
                "          \"regions\": [\n" +
                "            {\n" +
                "              \"country\": \"US\",\n" +
                "              \"name\": \"Massachusetts\",\n" +
                "              \"key\": \"3864\"\n" +
                "            }\n" +
                "          ],\n" +
                "          \"location_types\": [\n" +
                "            \"home\"\n" +
                "          ]\n" +
                "        },\n" +
                "        \"audience_network_positions\": [\n" +
                "          \"classic\"\n" +
                "        ],\n" +
                "        \"instagram_positions\": [\n" +
                "          \"stream\"\n" +
                "        ],\n" +
                "        \"age_min\": 35,\n" +
                "        \"device_platforms\": [\n" +
                "          \"mobile\",\n" +
                "          \"desktop\"\n" +
                "        ],\n" +
                "        \"facebook_positions\": [\n" +
                "          \"feed\",\n" +
                "          \"right_hand_column\",\n" +
                "          \"instant_article\",\n" +
                "          \"marketplace\"\n" +
                "        ],\n" +
                "        \"age_max\": 60,\n" +
                "        \"messenger_positions\": [\n" +
                "          \"messenger_home\"\n" +
                "        ]\n" +
                "      },\n" +
                "      \"id\": \"6095747941990\"\n" +
                "    }\n" +
                "  ]\n" +
                "}";

        List<AdSet> expected = new ArrayList<>();

        IDName idName = new IDName()
                .setFieldId("6003346088127")
                .setFieldName("Fitchburg State University");
        List<IDName> interests = Collections.singletonList(idName);
        List<FlexibleTargeting> flexibleTargetings = Collections.singletonList(new FlexibleTargeting().setFieldInterests(interests));

        TargetingGeoLocationRegion region = new TargetingGeoLocationRegion()
                .setFieldCountry("US")
                .setFieldKey("3864")
                .setFieldName("Massachusetts");
        TargetingGeoLocation geoLocation = new TargetingGeoLocation()
                .setFieldRegions(Collections.singletonList(region))
                .setFieldLocationTypes(Collections.singletonList("home"));

        Targeting targeting = new Targeting()
                .setFieldFlexibleSpec(flexibleTargetings)
                .setFieldPublisherPlatforms(Arrays.asList("facebook", "instagram", "audience_network", "messenger"))
                .setFieldGeoLocations(geoLocation)
                .setFieldAudienceNetworkPositions(Collections.singletonList("classic"))
                .setFieldInstagramPositions(Collections.singletonList("stream"))
                .setFieldAgeMin(35L)
                .setFieldAgeMax(60L)
                .setFieldDevicePlatforms(Arrays.asList(Targeting.EnumDevicePlatforms.VALUE_MOBILE, Targeting.EnumDevicePlatforms.VALUE_DESKTOP))
                .setFieldFacebookPositions(Arrays.asList("feed", "right_hand_column", "instant_article", "marketplace"))
                .setFieldMessengerPositions(Collections.singletonList("messenger_home"));

        AdSet adSet = new AdSet()
                .setFieldTargeting(targeting)
                .setFieldId("6095747941990");

        expected.add(adSet);

        JsonElement json = new JsonParser().parse(data);
        List<AdSet> actual = wordstreamAdSetDAO.parseAdSetFromJson(json).getAdSetData();

        JsonListEqualityChecker.getInstance().assertEqualLists(expected, actual);
    }
}
